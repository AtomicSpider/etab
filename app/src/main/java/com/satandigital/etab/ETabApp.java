package com.satandigital.etab;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.facebook.FacebookSdk;
import com.firebase.client.Firebase;

/**
 * Project : eTab
 * Created by Sanat Dutta on 4/27/2016.
 */
public class ETabApp extends Application {

    private String TAG = "ETabApp";

    //Data
    public static final String prefName = "eTAB_PREF";


    public static String userID = null;
    public static SharedPreferences mSharedPreferences;

    @Override
    public void onCreate() {
        super.onCreate();

        mSharedPreferences = getSharedPreferences(prefName, Context.MODE_PRIVATE);

        Firebase.setAndroidContext(this);
        Firebase.getDefaultConfig().setPersistenceEnabled(true);
        FacebookSdk.sdkInitialize(getApplicationContext());
    }
}
