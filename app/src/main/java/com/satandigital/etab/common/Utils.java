package com.satandigital.etab.common;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;
import android.view.inputmethod.InputMethodManager;

import com.satandigital.etab.ETabApp;
import com.satandigital.etab.models.CurrencyObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Currency;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Project : eTab
 * Created by Sanat Dutta on 4/28/2016.
 */
public class Utils {

    public static boolean isEmailValid(String email) {
        String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    public static void hideKeyboard(Context c, IBinder windowToken) {
        InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(windowToken, 0);
    }

    public static String capitalize(String str) {
        return capitalize(str, null);
    }

    public static String capitalize(String str, char[] delimiters) {
        int delimLen = (delimiters == null ? -1 : delimiters.length);
        if (str == null || str.length() == 0 || delimLen == 0) {
            return str;
        }
        int strLen = str.length();
        StringBuffer buffer = new StringBuffer(strLen);
        boolean capitalizeNext = true;
        for (int i = 0; i < strLen; i++) {
            char ch = str.charAt(i);

            if (isDelimiter(ch, delimiters)) {
                buffer.append(ch);
                capitalizeNext = true;
            } else if (capitalizeNext) {
                buffer.append(Character.toTitleCase(ch));
                capitalizeNext = false;
            } else {
                buffer.append(ch);
            }
        }
        return buffer.toString();
    }

    private static boolean isDelimiter(char ch, char[] delimiters) {
        if (delimiters == null) {
            return Character.isWhitespace(ch);
        }
        for (int i = 0, isize = delimiters.length; i < isize; i++) {
            if (ch == delimiters[i]) {
                return true;
            }
        }
        return false;
    }

    public static String doGreen(String mString) {
        return "<strong><font color=\"#009688\">" + mString + "</font></strong>";
    }

    public static String doBlue(String mString) {
        return "<strong><font color=\"#1976D2\">" + mString + "</font></strong>";
    }

    public static String doRed(String mString) {
        return "<strong><font color=\"#C64C3C\">" + mString + "</font></strong>";
    }

    public static void saveToSharedPreferences(String key, String sValue) {
        SharedPreferences.Editor mEditor = ETabApp.mSharedPreferences.edit();
        mEditor.putString(key, sValue);
        mEditor.commit();
    }

    public static void saveToSharedPreferences(String key, boolean bValue) {
        SharedPreferences.Editor mEditor = ETabApp.mSharedPreferences.edit();
        mEditor.putBoolean(key, bValue);
        mEditor.commit();
    }

    public static boolean readSharedPreferences(String key, boolean valueDefault) {
        return ETabApp.mSharedPreferences.getBoolean(key, valueDefault);
    }

    public static String readSharedPreferences(String key, String valueDefault) {
        return ETabApp.mSharedPreferences.getString(key, valueDefault);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static ArrayList<CurrencyObject> getCurrencies() {
        ArrayList<CurrencyObject> currencyList = new ArrayList<>();

        int mAPIVer = android.os.Build.VERSION.SDK_INT;
        if (mAPIVer >= 19) {
            Set<Currency> currencies = Currency.getAvailableCurrencies();
            for (Currency currency : currencies) {
                currencyList.add(new CurrencyObject(currency.getCurrencyCode(), currency.getSymbol()));
            }
        } else {
            ArrayList<String> currencies = new ArrayList<>();
            Locale[] mLocales = Locale.getAvailableLocales();
            for (Locale locale : mLocales) {
                String val = Currency.getInstance(locale).getCurrencyCode();
                if (!currencies.contains(val))
                    currencies.add(val);
                Collections.sort(currencies);
            }
            for (String currencyCode : currencies) {
                Currency currency = Currency.getInstance(currencyCode);
                currencyList.add(new CurrencyObject(currency.getCurrencyCode(), currency.getSymbol()));
            }
        }
        return currencyList;
    }
}
