package com.satandigital.etab.models;

/**
 * Project : eTab
 * Created by Sanat Dutta on 4/27/2016.
 */
public class IndSummeryObject {

    private String name = "";
    private double amount = 0;

    public IndSummeryObject(String name, double amount) {
        this.name = name;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
