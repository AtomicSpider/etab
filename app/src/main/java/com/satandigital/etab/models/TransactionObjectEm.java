package com.satandigital.etab.models;

/**
 * Project : eTab
 * Created by Sanat Dutta on 4/29/2016.
 */
public class TransactionObjectEm {

    private String contactName;
    private double amount;
    private String context;
    private String timestamp;

    public TransactionObjectEm() {
    }

    public String getContactName() {
        return contactName;
    }

    public double getAmount() {
        return amount;
    }

    public String getContext() {
        return context;
    }

    public String getTimestamp() {
        return timestamp;
    }
}
