package com.satandigital.etab.models;

/**
 * Created by Sanat Dutta on 3/3/2016.
 */

public class FacebookObject {

    private String facebookEmail;
    private String facebookUserId;
    private String facebookUserName;
    private String facebookAgeRange;
    private String facebookLink;
    private String facebookGender;
    private String facebookLocale;
    private String facebookDynamicPicture;
    private String facebookCDNPicture;
    private String facebookTimeZone;
    private String facebookUpdatedTime;
    private String facebookVerified;

    public void setAllToDefault(){
        this.facebookEmail = "NA";
        this.facebookUserId = "NA";
        this.facebookUserName = "NA";
        this.facebookAgeRange = "NA";
        this.facebookLink = "NA";
        this.facebookGender = "NA";
        this.facebookLocale = "NA";
        this.facebookDynamicPicture = "NA";
        this.facebookCDNPicture = "NA";
        this.facebookTimeZone = "NA";
        this.facebookUpdatedTime = "NA";
        this.facebookVerified = "NA";
    }

    public String getFacebookEmail() {
        return facebookEmail;
    }

    public void setFacebookEmail(String facebookEmail) {
        this.facebookEmail = facebookEmail;
    }

    public String getFacebookUserId() {
        return facebookUserId;
    }

    public void setFacebookUserId(String facebookUserId) {
        this.facebookUserId = facebookUserId;
    }

    public String getFacebookUserName() {
        return facebookUserName;
    }

    public void setFacebookUserName(String facebookUserName) {
        this.facebookUserName = facebookUserName;
    }

    public String getFacebookAgeRange() {
        return facebookAgeRange;
    }

    public void setFacebookAgeRange(String facebookAgeRange) {
        this.facebookAgeRange = facebookAgeRange;
    }

    public String getFacebookLink() {
        return facebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        this.facebookLink = facebookLink;
    }

    public String getFacebookGender() {
        return facebookGender;
    }

    public void setFacebookGender(String facebookGender) {
        this.facebookGender = facebookGender;
    }

    public String getFacebookLocale() {
        return facebookLocale;
    }

    public void setFacebookLocale(String facebookLocale) {
        this.facebookLocale = facebookLocale;
    }

    public String getFacebookDynamicPicture() {
        return facebookDynamicPicture;
    }

    public void setFacebookDynamicPicture(String facebookDynamicPicture) {
        this.facebookDynamicPicture = facebookDynamicPicture;
    }

    public String getFacebookCDNPicture() {
        return facebookCDNPicture;
    }

    public void setFacebookCDNPicture(String facebookCDNPicture) {
        this.facebookCDNPicture = facebookCDNPicture;
    }

    public String getFacebookTimeZone() {
        return facebookTimeZone;
    }

    public void setFacebookTimeZone(String facebookTimeZone) {
        this.facebookTimeZone = facebookTimeZone;
    }

    public String getFacebookUpdatedTime() {
        return facebookUpdatedTime;
    }

    public void setFacebookUpdatedTime(String facebookUpdatedTime) {
        this.facebookUpdatedTime = facebookUpdatedTime;
    }

    public String getFacebookVerified() {
        return facebookVerified;
    }

    public void setFacebookVerified(String facebookVerified) {
        this.facebookVerified = facebookVerified;
    }
}
