package com.satandigital.etab.models;

/**
 * Project : eTab
 * Created by Sanat Dutta on 4/29/2016.
 */
public class FirebaseUser {
    private String name;
    private String email;
    private String profilePicture;
    private boolean isFacebookLinked;

    public FirebaseUser() {
    }

    public FirebaseUser(String name, String email, String profilePicture, boolean isFacebookLinked) {
        this.name = name;
        this.email = email;
        this.profilePicture = profilePicture;
        this.isFacebookLinked = isFacebookLinked;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public boolean isFacebookLinked() {
        return isFacebookLinked;
    }
}
