package com.satandigital.etab.models;

/**
 * Project : eTab
 * Created by Sanat Dutta on 4/29/2016.
 */
public class ContactObject {

    private String contactName;
    private double amount;

    public ContactObject(String contactName, double amount) {
        this.contactName = contactName;
        this.amount = amount;
    }

    public String getContactName() {
        return contactName;
    }

    public double getAmount() {
        return amount;
    }
}
