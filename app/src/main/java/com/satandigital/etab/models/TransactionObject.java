package com.satandigital.etab.models;

/**
 * Project : eTab
 * Created by Sanat Dutta on 4/29/2016.
 */
public class TransactionObject {

    private String contactName;
    private double amount;
    private String context;
    private String timestamp;

    public TransactionObject(String contactName, double amount, String context, String timestamp) {
        this.contactName = contactName;
        this.amount = amount;
        this.context = context;
        this.timestamp = timestamp;
    }

    public String getContactName() {
        return contactName;
    }

    public double getAmount() {
        return amount;
    }

    public String getContext() {
        return context;
    }

    public String getTimestamp() {
        return timestamp;
    }
}
