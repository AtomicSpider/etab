package com.satandigital.etab.models;

/**
 * Project : eTab
 * Created by Sanat Dutta on 5/3/2016.
 */
public class CurrencyObject {

    private String currencyCode;
    private String currencySymbol;

    public CurrencyObject(String currencyCode, String currencySymbol) {
        this.currencyCode = currencyCode;
        this.currencySymbol = currencySymbol;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }
}
