package com.satandigital.etab.models;

// Created by Sanat Dutta on 1/17/2015.

public interface ScrollViewListener {

    void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy);

}
