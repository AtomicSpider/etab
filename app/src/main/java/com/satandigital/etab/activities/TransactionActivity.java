package com.satandigital.etab.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.satandigital.etab.ETabApp;
import com.satandigital.etab.R;
import com.satandigital.etab.common.RoundView;
import com.satandigital.etab.common.Utils;
import com.satandigital.etab.models.TransactionObjectEm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

/**
 * Project : eTab
 * Created by Sanat Dutta on 5/2/2016.
 */
public class TransactionActivity extends AppCompatActivity {

    private String TAG = "TransactionActivity";

    //Interfaces
    private transactionsAdapter mAdapter;

    //Views
    private ListView mListView;
    private CircularProgressView fetchingDataProgressBar;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    //Data
    Firebase myFirebaseRef;
    private boolean isDataFetching = false;
    private ArrayList<TransactionObjectEm> transactionObjects;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "TransactionActivity: onCreate()");

        transactionObjects = new ArrayList<>();
        myFirebaseRef = new Firebase(getResources().getString(R.string.firebase_url));

        setContentView(R.layout.activity_transaction);

        findViews();
        setUpActionBar();
        mAdapter = new transactionsAdapter();
        mListView.setAdapter(mAdapter);
        setUpSwipeRefreshLayout();
        getTransactions();
    }

    private void findViews() {
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.transactions_swipe_refresh_layout);
        mListView = (ListView) findViewById(R.id.transactions_listview);
        fetchingDataProgressBar = (CircularProgressView) findViewById(R.id.fetchingDataProgressBar);
    }

    private void setUpActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle(getResources().getString(R.string.transactions));
    }

    private void getTransactions() {
        isDataFetching = true;

        Log.i(TAG, "Fetching Comments");
        Firebase transactionRef = myFirebaseRef.child("users").child(ETabApp.userID).child("transactions");
        //ToDo limit the query
        Query transactionQuery = transactionRef.orderByChild("timestamp");

        transactionQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i(TAG, "TransactionObject data received. Length: " + dataSnapshot.getChildrenCount());

                fetchingDataProgressBar.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);
                isDataFetching = false;

                if (dataSnapshot.getChildrenCount() > 0) {
                    Log.i(TAG, "Transaction data exists");
                    mSwipeRefreshLayout.setBackgroundColor(ContextCompat.getColor(TransactionActivity.this, R.color.white));
                    transactionObjects.clear();
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        Log.d(TAG, "Data: " + postSnapshot.getValue());
                        TransactionObjectEm post = postSnapshot.getValue(TransactionObjectEm.class);
                        transactionObjects.add(post);
                    }
                    Collections.reverse(transactionObjects);
                    mAdapter.notifyDataSetChanged();
                } else {
                    Log.i(TAG, "Data does not exists");
                    transactionObjects.clear();
                    mAdapter.notifyDataSetChanged();
                    mSwipeRefreshLayout.setBackgroundColor(ContextCompat.getColor(TransactionActivity.this, R.color.transparent));
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e(TAG, "" + firebaseError.getMessage());
                Toast.makeText(TransactionActivity.this, "" + firebaseError.getMessage(), Toast.LENGTH_SHORT).show();

                fetchingDataProgressBar.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);
                isDataFetching = false;
            }
        });
    }

    private void setUpSwipeRefreshLayout() {
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.i(TAG, "onRefresh");
                if (!isDataFetching) {
                    getTransactions();
                } else {
                    Log.i(TAG, "Data Fetch Ongoing");
                }
            }
        });
    }


    private class transactionsAdapter extends BaseAdapter {

        private LayoutInflater mLayoutInflater;
        ColorGenerator mColorGenerator;

        private transactionsAdapter() {
            mLayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mColorGenerator = ColorGenerator.MATERIAL;
        }

        @Override
        public int getCount() {
            return transactionObjects.size();
        }

        @Override
        public Object getItem(int position) {
            return transactionObjects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View mView, ViewGroup parent) {
            if (mView == null)
                mView = mLayoutInflater.inflate(R.layout.item_transaction_list, parent, false);

            ViewHolder mViewHolder = new ViewHolder();

            mViewHolder.transRoundView = (RoundView) mView.findViewById(R.id.item_transaction_icon_rv);
            mViewHolder.transIconTV = (TextView) mView.findViewById(R.id.item_transaction_icon_tv);
            mViewHolder.transContactNameTV = (TextView) mView.findViewById(R.id.item_transaction_name_tv);
            mViewHolder.transTimestampTV = (TextView) mView.findViewById(R.id.item_transaction_timestamp_tv);
            mViewHolder.transMessageTV = (TextView) mView.findViewById(R.id.item_transaction_message_tv);
            mViewHolder.transContextTV = (TextView) mView.findViewById(R.id.item_transaction_context_tv);

            String name = Utils.capitalize(transactionObjects.get(position).getContactName());
            double amount = transactionObjects.get(position).getAmount();

            mViewHolder.transRoundView.setRoundColor(mColorGenerator.getRandomColor());
            mViewHolder.transIconTV.setText((name.substring(0, 1)).toUpperCase());
            mViewHolder.transContactNameTV.setText(name);

            long timestamp = Long.valueOf(transactionObjects.get(position).getTimestamp()) * 1000;
            Date postDate = new Date(timestamp);
            mViewHolder.transTimestampTV.setText(android.text.format.DateFormat.format("dd", postDate) + " " + android.text.format.DateFormat.format("MMM", postDate) + " " + android.text.format.DateFormat.format("yy", postDate));

            if (amount < 0)
                mViewHolder.transMessageTV.setText(Html.fromHtml("You owe " + name + " $ " + Utils.doRed(String.valueOf(-amount))));
            else
                mViewHolder.transMessageTV.setText(Html.fromHtml(name + " owes you $ " + Utils.doGreen(String.valueOf(amount))));

            if (!transactionObjects.get(position).getContext().equals("")) {
                mViewHolder.transContextTV.setVisibility(View.VISIBLE);
                mViewHolder.transContextTV.setText("Context: " + transactionObjects.get(position).getContext());
            }

            return mView;
        }

        private class ViewHolder {
            RoundView transRoundView;
            TextView transIconTV, transContactNameTV, transTimestampTV, transMessageTV, transContextTV;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case android.R.id.home:
                finish();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
