package com.satandigital.etab.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.satandigital.etab.ETabApp;
import com.satandigital.etab.R;
import com.satandigital.etab.common.Utils;
import com.satandigital.etab.models.FacebookObject;
import com.satandigital.etab.models.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Map;

/**
 * Project : eTab
 * Created by Sanat Dutta on 4/27/2016.
 */
public class LoginActivity extends AppCompatActivity {

    private String TAG = "LoginActivity";

    //Interfaces
    CallbackManager mCallbackManager;
    LoginButton fbLoginButton;

    //Views
    RelativeLayout signInTab, signUpTab, signInLayout, signUpLayout;
    TextView signInTabText, signUpTabText;
    View signInTabView, signUpTabView;
    TextView loginEmailButton, loginFacebookButton, loginTwitterButton, loginGoogleButton, signUpButton;
    EditText nameET, emailET, passET, rePassET;
    MaterialDialog progressDialog;

    //Data
    private int currentTab = 0;
    private Firebase myFirebaseRef;
    FacebookObject mFacebookObject;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "LoginActivity: onCreate()");
        myFirebaseRef = new Firebase(getResources().getString(R.string.firebase_url));

        setContentView(R.layout.activity_login);
        findViews();

        setUpOnClickListeners();
        setUpFacebookLogin();
    }

    private void setUpFacebookLogin() {
        fbLoginButton = (LoginButton) findViewById(R.id.fb_login_button);
        fbLoginButton.setReadPermissions(Arrays.asList("public_profile", "email"));

        mCallbackManager = CallbackManager.Factory.create();

        fbLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i(TAG, "User logged in through Facebook!");
                //getUserDetailsFromFacebook();
                logInToFirebaseViaFb();
            }

            @Override
            public void onCancel() {
                Log.i(TAG, "User cancelled facebook login");
            }

            @Override
            public void onError(FacebookException exception) {
                Log.e(TAG, "Facebook login Error: " + exception.getMessage());
                Toast.makeText(LoginActivity.this, "Facebook login Error: " + exception.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void logInToFirebaseViaFb() {
        myFirebaseRef.authWithOAuthToken("facebook", AccessToken.getCurrentAccessToken().getToken(), new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
                Log.i(TAG, "Facebook firebase authenticated: " + authData);
            }

            @Override
            public void onAuthenticationError(FirebaseError firebaseError) {
                //ToDo
                Log.e(TAG, "Facebook firebase authentication Error: " + firebaseError.getMessage());
                Toast.makeText(LoginActivity.this, "Facebook firebase authentication Error: " + firebaseError.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getUserDetailsFromFacebook() {
        GraphRequest mGraphRequest = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    mFacebookObject = new FacebookObject();
                    mFacebookObject.setAllToDefault();
                    String minAge = "0", maxAge = "0";

                    //ToDo remove
                    Log.d(TAG, "" + object);

                    if (object.has("email"))
                        mFacebookObject.setFacebookEmail(object.getString("email"));
                    if (object.has("id"))
                        mFacebookObject.setFacebookUserId(object.getString("id"));
                    if (object.has("name"))
                        mFacebookObject.setFacebookUserName(object.getString("name"));
                    if (object.getJSONObject("age_range").has("min"))
                        minAge = object.getJSONObject("age_range").getString("min");
                    if (object.getJSONObject("age_range").has("max"))
                        maxAge = object.getJSONObject("age_range").getString("max");
                    mFacebookObject.setFacebookAgeRange(minAge + "-" + maxAge);
                    if (object.has("link"))
                        mFacebookObject.setFacebookLink(object.getString("link"));
                    if (object.has("gender"))
                        mFacebookObject.setFacebookGender(object.getString("gender"));
                    if (object.has("locale"))
                        mFacebookObject.setFacebookLocale(object.getString("locale"));
                    if (object.has("id"))
                        mFacebookObject.setFacebookDynamicPicture("http://graph.facebook.com/" + mFacebookObject.getFacebookUserId() + "/picture?type=large");
                    if (object.getJSONObject("picture").has("data")) {
                        if (object.getJSONObject("picture").getJSONObject("data").has("url"))
                            mFacebookObject.setFacebookCDNPicture(object.getJSONObject("picture").getJSONObject("data").getString("url"));
                    }
                    if (object.has("timezone"))
                        mFacebookObject.setFacebookTimeZone(object.getString("timezone"));
                    if (object.has("updated_time"))
                        mFacebookObject.setFacebookUpdatedTime(object.getString("updated_time"));
                    if (object.has("verified"))
                        mFacebookObject.setFacebookVerified(object.getString("verified"));
                } catch (JSONException e) {
                    progressDialog.dismiss();
                    mFacebookObject = null;
                    Log.e(TAG, "Error decoding Facebook Data");
                    //Toast.makeText(getActivity(), "Oops, Something went wrong", Toast.LENGTH_SHORT).show();
                }

                //if (mFacebookObject != null) saveFacebookUserDataToParse(mFacebookObject);
                //ToDo if no email
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, name, age_range, link, gender, locale, picture, timezone, updated_time, verified, email");
        mGraphRequest.setParameters(parameters);
        mGraphRequest.executeAsync();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void findViews() {
        signInTab = (RelativeLayout) findViewById(R.id.sign_in_tab);
        signUpTab = (RelativeLayout) findViewById(R.id.sign_up_tab);
        signInLayout = (RelativeLayout) findViewById(R.id.sign_in_layout);
        signUpLayout = (RelativeLayout) findViewById(R.id.sign_up_layout);
        signInTabText = (TextView) findViewById(R.id.sign_in_tab_text);
        signUpTabText = (TextView) findViewById(R.id.sign_up_tab_text);
        signInTabView = findViewById(R.id.sign_in_tab_view);
        signUpTabView = findViewById(R.id.sign_up_tab_view);
        loginEmailButton = (TextView) findViewById(R.id.login_email_button);
        loginFacebookButton = (TextView) findViewById(R.id.login_facebook_button);
        loginTwitterButton = (TextView) findViewById(R.id.login_twitter_button);
        loginGoogleButton = (TextView) findViewById(R.id.login_google_button);
        signUpButton = (TextView) findViewById(R.id.sign_up_button);
        nameET = (EditText) findViewById(R.id.name_et);
        emailET = (EditText) findViewById(R.id.email_et);
        passET = (EditText) findViewById(R.id.pass_et);
        rePassET = (EditText) findViewById(R.id.re_pass_et);
    }

    private void setUpOnClickListeners() {
        signInTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentTab != 0) {
                    currentTab = 0;
                    signInTabText.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.tab_dark));
                    signInTabText.setTypeface(Typeface.DEFAULT_BOLD);
                    signInTabView.setBackgroundColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimary));
                    signUpTabText.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.tab_light));
                    signUpTabText.setTypeface(Typeface.DEFAULT);
                    signUpTabView.setBackgroundColor(ContextCompat.getColor(LoginActivity.this, R.color.white));

                    signInLayout.setVisibility(View.VISIBLE);
                    signUpLayout.setVisibility(View.GONE);

                    Utils.hideKeyboard(LoginActivity.this, signInTab.getWindowToken());
                }
            }
        });
        signUpTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentTab == 0) {
                    currentTab = 1;
                    signInTabText.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.tab_light));
                    signInTabText.setTypeface(Typeface.DEFAULT);
                    signInTabView.setBackgroundColor(ContextCompat.getColor(LoginActivity.this, R.color.white));
                    signUpTabText.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.tab_dark));
                    signUpTabText.setTypeface(Typeface.DEFAULT_BOLD);
                    signUpTabView.setBackgroundColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimary));

                    signInLayout.setVisibility(View.GONE);
                    signUpLayout.setVisibility(View.VISIBLE);
                }
            }
        });
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Sign Up button tapped");

                String errorString = null;
                String rePass = rePassET.getText().toString();
                String pass = passET.getText().toString();
                String email = emailET.getText().toString().trim();
                String name = nameET.getText().toString().trim();

                if (!rePass.equals(pass)) {
                    errorString = "Passwords do not match";
                }
                if (pass.length() < 8) {
                    errorString = "Password must be at least 8 characters";
                }
                if (pass.length() > 48) {
                    errorString = "Password must not exceed 48 characters";
                }
                if (pass.equals("")) {
                    errorString = "Password field is empty";
                }
                if (pass.contains(" ")) {
                    errorString = "Password cannot contain blank spaces";
                }
                if (!Utils.isEmailValid(email)) {
                    errorString = "Email is not valid";
                }
                if (email.equals("")) {
                    errorString = "Email field is empty";
                }
                if (email.contains(" ")) {
                    errorString = "Email cannot contain blank spaces";
                }
                if (name.length() > 20) {
                    errorString = "Password must not exceed 20 characters";
                }
                if (name.equals("")) {
                    errorString = "Name field is empty";
                }

                if (errorString == null) {
                    signUpFirebase(name, email, pass);
                } else
                    Toast.makeText(LoginActivity.this, "" + errorString, Toast.LENGTH_LONG).show();
            }
        });
        loginEmailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Email log in button tapped.");
                showSignInDialog();
            }
        });
        loginFacebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ToDo if accesToken then directly login else
                fbLoginButton.performClick();
            }
        });
    }

    private void showSignInDialog() {
        final MaterialDialog mDialog = new MaterialDialog.Builder(this)
                .title("SIGN IN")
                .titleColorRes(R.color.teal_700)
                .theme(Theme.LIGHT)
                .customView(R.layout.dialog_sign_in, true)
                .cancelable(true)
                .build();

        View mView = mDialog.getCustomView();
        final EditText emailET = (EditText) mView.findViewById(R.id.email_et);
        final EditText passET = (EditText) mView.findViewById(R.id.pass_et);
        TextView signInButton = (TextView) mView.findViewById(R.id.sign_in_button);
        TextView forgotPassButton = (TextView) mView.findViewById(R.id.forgot_pass_button);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Logging in using email");
                mDialog.dismiss();
                signInFirebase(null, null, emailET.getText().toString(), passET.getText().toString());
            }
        });
        forgotPassButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Forgot password.");
                mDialog.dismiss();
                showResetDialog();
            }
        });

        mDialog.show();
    }

    private void showResetDialog() {
        final MaterialDialog mDialog = new MaterialDialog.Builder(this)
                .title("Reset Password")
                .titleColorRes(R.color.teal_700)
                .theme(Theme.LIGHT)
                .customView(R.layout.dialog_reset_password, true)
                .cancelable(true)
                .build();

        View mView = mDialog.getCustomView();
        final EditText emailET = (EditText) mView.findViewById(R.id.email_et);
        TextView resetPassButton = (TextView) mView.findViewById(R.id.reset_pass_button);

        resetPassButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Resetting password");
                String email = emailET.getText().toString().trim();
                mDialog.dismiss();
                resetPasswordFirebase(email);
            }
        });

        mDialog.show();
    }

    private void resetPasswordFirebase(String email) {
        showProgressDialog("Sending reset instructions to your registered email...");
        myFirebaseRef.resetPassword(email, new Firebase.ResultHandler() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "Successfully reset password");
                progressDialog.dismiss();
                showPostResetDialog();
            }

            @Override
            public void onError(FirebaseError firebaseError) {
                Log.e(TAG, "Error password reset, Error: " + firebaseError.getMessage() + "Error Code: " + firebaseError.getCode());
                Toast.makeText(LoginActivity.this, "" + firebaseError.getMessage(), Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        });
    }

    private void showPostResetDialog() {
        new MaterialDialog.Builder(this)
                .theme(Theme.LIGHT)
                .content("Please check your email for reset instructions.")
                .cancelable(true)
                .positiveText("Okay")
                .show();
    }

    private void signUpFirebase(final String name, final String email, final String pass) {
        Log.i(TAG, "Signing up using firebase");
        showProgressDialog("Creating Account...");
        myFirebaseRef.createUser(email, pass, new Firebase.ValueResultHandler<Map<String, Object>>() {
            @Override
            public void onSuccess(Map<String, Object> stringObjectMap) {
                Log.i(TAG, "Successfully created user account with uid: " + stringObjectMap.get("uid"));
                signInFirebase("signup", name, email, pass);
            }

            @Override
            public void onError(FirebaseError firebaseError) {
                Log.e(TAG, "Error user sign up, Error: " + firebaseError.getMessage() + "Error Code: " + firebaseError.getCode());
                Toast.makeText(LoginActivity.this, "" + firebaseError.getMessage(), Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        });
    }

    private void signInFirebase(final String parent, final String name, String email, String pass) {
        if (parent == null) showProgressDialog("Logging in...");
        else progressDialog.setContent("Logging in...");
        myFirebaseRef.authWithPassword(email, pass, new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticated(final AuthData authData) {
                Log.i(TAG, "Successfully logged in. User ID: " + authData.getUid());

                if (parent != null) { //ToDo first time login or if the user does not exist or if user had incomplete detail
                    Firebase userRef = myFirebaseRef.child("users").child(authData.getUid());
                    FirebaseUser mFirebaseUser = new FirebaseUser(name, authData.getProviderData().get("email").toString(), authData.getProviderData().get("profileImageURL").toString(), false);
                    userRef.setValue(mFirebaseUser, new Firebase.CompletionListener() {
                        @Override
                        public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                            if (firebaseError == null) {
                                Log.i(TAG, "User added successfully");
                                ETabApp.userID = authData.getUid();
                                //ToDo go to main
                            } else {
                                //ToDo Logout
                                Log.i(TAG, "User could not be added. Error: " + firebaseError.getMessage());
                                Toast.makeText(LoginActivity.this, "User could not be added. Error: " + firebaseError.getMessage(), Toast.LENGTH_LONG).show();
                            }
                            progressDialog.dismiss();
                        }
                    });
                } else {
                    //ToDo parent null update user
                }
            }

            @Override
            public void onAuthenticationError(FirebaseError firebaseError) {
                Log.e(TAG, "Error user login, Error: " + firebaseError.getMessage() + "Error Code: " + firebaseError.getCode());
                Toast.makeText(LoginActivity.this, "" + firebaseError.getMessage(), Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        });
    }

    private void showProgressDialog(String message) {
        progressDialog = new MaterialDialog.Builder(this)
                .content(message)
                .cancelable(false)
                .theme(Theme.LIGHT)
                .progress(true, 0)
                .show();
    }
}
