package com.satandigital.etab.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.MutableData;
import com.firebase.client.ValueEventListener;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.satandigital.etab.ETabApp;
import com.satandigital.etab.R;
import com.satandigital.etab.common.RateTheApp;
import com.satandigital.etab.common.RoundView;
import com.satandigital.etab.common.Utils;
import com.satandigital.etab.models.ContactObject;
import com.satandigital.etab.models.CurrencyObject;
import com.satandigital.etab.models.TransactionObject;

import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.List;

/**
 * Project : eTab
 * Created by Sanat Dutta on 4/27/2015.
 */

public class MainActivity extends AppCompatActivity {

    private String TAG = "MainActivity";

    //Interfaces
    private Toolbar mToolbar;
    private ActionBar mActionBar;
    private ActionBarDrawerToggle mDrawerToggle;
    private IndSummeryAdapter mIndSummeryAdapter;

    //Views
    private ExpandableHeightListView mIndSummeryListView;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private View mHeaderView;
    private com.getbase.floatingactionbutton.FloatingActionButton actionAddEntry;
    private ImageView currentUserImage;
    private TextView userBalanceTV, userBalanceHeaderTV, userName, userEmail;
    private View positiveAction;

    //Data
    private Firebase myFirebaseRef;

    private String feedbackDialogSubject, feedbackDialogMessage;
    private String amountEntryDialog, nameEntryDialog, contextEntryDialog;

    boolean isBeingReceived = true;

    private ArrayList<ContactObject> mContactObjectObjects;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "MainActivity: onCreate()");
        myFirebaseRef = new Firebase(getResources().getString(R.string.firebase_url));
        mContactObjectObjects = new ArrayList<>();

        setContentView(R.layout.activity_main);
        findViews();
        setCustomFonts();

        setUpActionBar();
        setUpNavigationDrawer();
        setDrawerOnClickListeners();
        setHeader();
        setUpIndSummeryListView();

        //toggleAppropriateNavItems();
        setUpOnClickListeners();
        setAppRate();

        initializeFirebase();
        monitorUserSession();
        fetchData();
    }

    private void setCustomFonts() {
        Typeface face = Typeface.createFromAsset(getAssets(), "font/Roboto-Light.ttf");
        userBalanceTV.setTypeface(face);
    }

    private void initializeFirebase() {
        Firebase myFirebaseRef = new Firebase(getResources().getString(R.string.firebase_url));
    }

    private void monitorUserSession() {
        myFirebaseRef.addAuthStateListener(new Firebase.AuthStateListener() {
            @Override
            public void onAuthStateChanged(AuthData authData) {
                if (authData != null) {
                    Log.i(TAG, "" + authData);
                } else {
                    myFirebaseRef.unauth();
                    finish();
                    //ToDo fo to loginactivity
                }
            }
        });
    }

    private void fetchData() {
        Firebase userRef = myFirebaseRef.child("users").child(ETabApp.userID).child("balance");
        Firebase contactsRef = myFirebaseRef.child("users").child(ETabApp.userID).child("contacts");

        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Log.i(TAG, "User balance updated: " + snapshot.getValue());
                double balance;
                if (snapshot.getValue() != null)
                    balance = Double.valueOf(snapshot.getValue().toString());
                else balance = 0;
                if (balance < 0) {
                    userBalanceTV.setText(String.valueOf(-balance));
                    userBalanceHeaderTV.setText("YOU OWE:");
                    userBalanceTV.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.box_text_red));
                } else {
                    userBalanceTV.setText(String.valueOf(balance));
                    userBalanceHeaderTV.setText("PEOPLE OWE YOU:");
                    userBalanceTV.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.box_text_green));
                }

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e(TAG, "" + firebaseError.getMessage());
            }
        });

        contactsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                mContactObjectObjects.clear();
                Log.d(TAG, "" + snapshot.getValue());

                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    mContactObjectObjects.add(new ContactObject(postSnapshot.getKey(), Double.valueOf(postSnapshot.getValue().toString())));
                }
                Log.i(TAG, "ContactObject objects updated with " + snapshot.getChildrenCount() + "objects");
                mIndSummeryAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.out.println("The read failed: " + firebaseError.getMessage());
            }
        });
    }

    private void findViews() {
        actionAddEntry = (com.getbase.floatingactionbutton.FloatingActionButton) findViewById(R.id.action_add_entry);
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        mHeaderView = mNavigationView.getHeaderView(0);
        currentUserImage = (ImageView) mHeaderView.findViewById(R.id.user_image_view);
        userBalanceTV = (TextView) findViewById(R.id.user_balance_tv);
        userBalanceHeaderTV = (TextView) findViewById(R.id.user_balance_header_tv);
        userName = (TextView) mHeaderView.findViewById(R.id.username_text_view);
        userEmail = (TextView) mHeaderView.findViewById(R.id.user_email_text_view);

        mIndSummeryListView = (ExpandableHeightListView) findViewById(R.id.individual_summery_listview);
        mIndSummeryListView.setExpanded(true);
        mIndSummeryListView.setFocusable(false);
    }

    private void setUpActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setHomeButtonEnabled(true);
        mActionBar.setTitle(getResources().getString(R.string.app_name));
    }

    private void setUpNavigationDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Log.i(TAG, "Drawer Opened");
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Log.i(TAG, "Drawer Closed");
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    private void setDrawerOnClickListeners() {

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                switch (menuItem.getItemId()) {
                    case R.id.nav_currency:
                        showCurrencyChooserDialog();
                        break;
                    /*case R.id.nav_choose_college:
                        showCollegeChooserDialog();
                        break;
                    case R.id.nav_about:
                        showAboutDialog();
                        break;
                    case R.id.nav_share:
                        shareApp();
                        break;
                    case R.id.nav_rate:
                        Intent mIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.satandigital.quizmate"));
                        startActivity(mIntent);
                        setOptOut(MainScreenActivity.this, true);
                        break;
                    case R.id.nav_feedback:
                        showFeedbackDialog();
                        break;
                    case R.id.nav_contact:
                        contactIntent();
                        break;*/
                }

                mDrawerLayout.closeDrawer(GravityCompat.START);
                return true;
            }
        });
    }

    private void showCurrencyChooserDialog() {
        ArrayList<CurrencyObject> currencyList = Utils.getCurrencies();

        Log.i(TAG, "Size: " + currencyList.size());

        for (CurrencyObject mCurrencyObject : currencyList) {
            Log.d(TAG, mCurrencyObject.getCurrencySymbol() + " " + mCurrencyObject.getCurrencyCode());
        }

        /*MaterialDialog mDialog = new MaterialDialog.Builder(this)
                .theme(Theme.LIGHT)
                .title("Set Currency")
                .titleColorRes(R.color.teal_700)
                .customView(R.layout.dialog_set_currency, true)
                .cancelable(true)
                .positiveText("Set")
                .positiveColorRes(R.color.colorPrimary)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //ToDo
                    }
                })
                .build();

        View mView = mDialog.getCustomView();
        ListView mListView = (ListView) mView.findViewById(R.id.currency_listview);
        CurrencyAdapter mCurrencyAdapter = new CurrencyAdapter(currencyList);
        mListView.setAdapter(mCurrencyAdapter);

        mDialog.show();*/
    }

    private void setHeader() {
        /*if (QuizMateApp.currentDisplayName != null)
            userName.setText(QuizMateApp.currentDisplayName);
        else userName.setText("Anonymous");
        if (QuizMateApp.currentInstituteName != null)
            userCollege.setText(QuizMateApp.currentInstituteName);
        else userCollege.setText("College");*/
    }

    private void setUpIndSummeryListView() {
        mIndSummeryAdapter = new IndSummeryAdapter();
        mIndSummeryListView.setAdapter(mIndSummeryAdapter);
    }

    private void setUpOnClickListeners() {
        actionAddEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Add new entry button");
                showAddNewEntryDialog();
            }
        });

        userBalanceTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Launching Transaction Activity, Parent: Balance Textview");
                Intent mIntent = new Intent(MainActivity.this, TransactionActivity.class);
                startActivity(mIntent);
            }
        });
    }

    private void showAddNewEntryDialog() {
        contextEntryDialog = "";
        isBeingReceived = true;
        MaterialDialog mDialog = new MaterialDialog.Builder(this)
                .theme(Theme.LIGHT)
                .customView(R.layout.dialog_add_entry, true)
                .cancelable(true)
                .positiveText("Submit")
                .positiveColorRes(R.color.colorPrimary)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Log.i(TAG, "Entry added from dialog. " + amountEntryDialog + " " + nameEntryDialog + " " + contextEntryDialog);
                        if (isBeingReceived)
                            saveEntry(Double.valueOf(amountEntryDialog), nameEntryDialog, contextEntryDialog);
                        else
                            saveEntry(Double.valueOf("-" + amountEntryDialog), nameEntryDialog, contextEntryDialog);
                    }
                })
                .build();

        View mView = mDialog.getCustomView();
        final TextView greenTV = (TextView) mView.findViewById(R.id.greenTV);
        final TextView redTV = (TextView) mView.findViewById(R.id.redTV);
        final TextView conTV = (TextView) mView.findViewById(R.id.conTV);
        final EditText amountET = (EditText) mView.findViewById(R.id.amountET);
        final EditText nameET = (EditText) mView.findViewById(R.id.nameET);
        final EditText contextET = (EditText) mView.findViewById(R.id.contextET);

        positiveAction = mDialog.getActionButton(DialogAction.POSITIVE);
        amountET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 0) {
                    amountEntryDialog = s.toString().trim();
                    if (nameET.getText().toString().trim().length() > 0) {
                        contextEntryDialog = contextET.getText().toString().trim();
                        positiveAction.setEnabled(true);
                    }
                } else positiveAction.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        nameET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 0) {
                    nameEntryDialog = s.toString().trim().toLowerCase();
                    if (amountET.getText().toString().trim().length() > 0) {
                        contextEntryDialog = contextET.getText().toString().trim();
                        positiveAction.setEnabled(true);
                    }
                } else positiveAction.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        contextET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                contextEntryDialog = s.toString().trim();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        greenTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Add Entry Dialog. Green Button");
                isBeingReceived = true;
                greenTV.setBackgroundResource(R.drawable.add_entry_green_fill);
                redTV.setBackgroundResource(R.drawable.add_entry_red_stroke);
                greenTV.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.white));
                redTV.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.card_text));
                conTV.setText("TO");
                amountET.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.box_text_green));
                nameET.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.box_text_green));
                contextET.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.box_text_green));
            }
        });

        redTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Add Entry Dialog. Green Button");
                isBeingReceived = false;
                greenTV.setBackgroundResource(R.drawable.add_entry_green_stroke);
                redTV.setBackgroundResource(R.drawable.add_entry_red_fill);
                greenTV.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.card_text));
                redTV.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.white));
                conTV.setText("FROM");
                amountET.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.box_text_red));
                nameET.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.box_text_red));
                contextET.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.box_text_red));
            }
        });

        mDialog.show();
        positiveAction.setEnabled(false);
    }

    private void saveEntry(double amountEntryDialog, String nameEntryDialog, String contextEntryDialog) {
        Firebase transactionRef = myFirebaseRef.child("users").child(ETabApp.userID).child("transactions");
        long timestamp = (new Date()).getTime() / 1000;
        TransactionObject mTransactionObject = new TransactionObject(nameEntryDialog, amountEntryDialog, contextEntryDialog, String.valueOf(timestamp));
        transactionRef.push().setValue(mTransactionObject);

        updateContact(amountEntryDialog, nameEntryDialog);
    }

    private void updateContact(final double amountEntryDialog, String nameEntryDialog) {
        Log.i(TAG, "Updating contact, " + nameEntryDialog);
        Firebase contactRef = myFirebaseRef.child("users").child(ETabApp.userID).child("contacts").child(nameEntryDialog);
        contactRef.runTransaction(new com.firebase.client.Transaction.Handler() {
            @Override
            public com.firebase.client.Transaction.Result doTransaction(MutableData mutableData) {
                if (mutableData.getValue() == null) {
                    Log.i(TAG, "ContactObject had no previous entry. Updating to " + amountEntryDialog);
                    mutableData.setValue(amountEntryDialog);
                } else {
                    mutableData.setValue(Double.valueOf(mutableData.getValue().toString()) + amountEntryDialog);
                    Log.i(TAG, "ContactObject had previous entries. Updating from " + amountEntryDialog + "to " + mutableData.getValue());
                }
                return com.firebase.client.Transaction.success(mutableData);
            }

            @Override
            public void onComplete(FirebaseError firebaseError, boolean b, DataSnapshot dataSnapshot) {
                if (firebaseError != null)
                    Log.e(TAG, "FirebaseError: " + firebaseError.getMessage());
                Log.i(TAG, "ContactObject update complete");
            }
        });
        updateUserBalance(amountEntryDialog);
    }

    private void updateUserBalance(final double amountEntryDialog) {
        Firebase userBalanceRef = myFirebaseRef.child("users").child(ETabApp.userID).child("balance");
        userBalanceRef.runTransaction(new com.firebase.client.Transaction.Handler() {
            @Override
            public com.firebase.client.Transaction.Result doTransaction(MutableData mutableData) {
                if (mutableData.getValue() == null) {
                    Log.i(TAG, "User had no previous entry. Updating to " + amountEntryDialog);
                    mutableData.setValue(amountEntryDialog);
                } else {
                    mutableData.setValue(Double.valueOf(mutableData.getValue().toString()) + amountEntryDialog);
                    Log.i(TAG, "User had previous entries. Updating from " + amountEntryDialog + "to " + mutableData.getValue());
                }
                return com.firebase.client.Transaction.success(mutableData);
            }

            @Override
            public void onComplete(FirebaseError firebaseError, boolean b, DataSnapshot dataSnapshot) {
                if (firebaseError != null)
                    Log.e(TAG, "FirebaseError: " + firebaseError.getMessage());
                Log.i(TAG, "ENTRY COMPLETED");
                Toast.makeText(MainActivity.this, "Entry Updated", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setAppRate() {
        RateTheApp.onStart(this);
        RateTheApp.showRateDialogIfNeeded(this);
    }

    /*************************
     * Joint Methods
     **********************************************/

    private class IndSummeryAdapter extends BaseAdapter {

        private LayoutInflater mLayoutInflater;
        ColorGenerator mColorGenerator;

        private IndSummeryAdapter() {
            mLayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mColorGenerator = ColorGenerator.MATERIAL;
        }

        @Override
        public int getCount() {
            return mContactObjectObjects.size();
        }

        @Override
        public Object getItem(int position) {
            return mContactObjectObjects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View mView = mLayoutInflater.inflate(R.layout.item_individual_balance_list, parent, false);
            ViewHolder mViewHolder = new ViewHolder();

            mViewHolder.iconRoundView = (RoundView) mView.findViewById(R.id.ind_summery_icon_rg);
            mViewHolder.iconTextView = (TextView) mView.findViewById(R.id.ind_summery_icon_tv);
            mViewHolder.nameTextView = (TextView) mView.findViewById(R.id.ind_summery_name_tv);
            mViewHolder.messageTextView = (TextView) mView.findViewById(R.id.ind_summery_message_tv);

            String name = Utils.capitalize(mContactObjectObjects.get(position).getContactName());
            double amount = mContactObjectObjects.get(position).getAmount();

            mViewHolder.iconRoundView.setRoundColor(mColorGenerator.getRandomColor());
            mViewHolder.iconTextView.setText((name.substring(0, 1)).toUpperCase());

            mViewHolder.nameTextView.setText(name);

            if (amount < 0)
                mViewHolder.messageTextView.setText(Html.fromHtml("You owe " + name + " $ " + Utils.doRed(String.valueOf(-amount))));
            else
                mViewHolder.messageTextView.setText(Html.fromHtml(name + " owes you $ " + Utils.doGreen(String.valueOf(amount))));

            return mView;
        }

        private class ViewHolder {
            private RoundView iconRoundView;
            private TextView iconTextView, nameTextView, messageTextView;
        }
    }

    private class CurrencyAdapter extends BaseAdapter {

        private LayoutInflater mLayoutInflater;
        private ArrayList<CurrencyObject> mCurrencyObjects;

        private CurrencyAdapter(ArrayList<CurrencyObject> mCurrencyObjects) {
            mLayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.mCurrencyObjects = mCurrencyObjects;
        }

        @Override
        public int getCount() {
            return mCurrencyObjects.size();
        }

        @Override
        public Object getItem(int position) {
            return mContactObjectObjects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View mView, ViewGroup parent) {
            if (mView == null)
                mView = mLayoutInflater.inflate(R.layout.item_individual_balance_list, parent, false);
            ViewHolder mViewHolder = new ViewHolder();

            mViewHolder.symbolTV = (TextView) mView.findViewById(R.id.currency_symbol);
            mViewHolder.codeTV = (TextView) mView.findViewById(R.id.currency_code);

            mViewHolder.symbolTV.setText(mCurrencyObjects.get(position).getCurrencySymbol());
            mViewHolder.codeTV.setText(mCurrencyObjects.get(position).getCurrencyCode());

            return mView;
        }

        private class ViewHolder {
            private TextView symbolTV, codeTV;
        }
    }

    /*************************
     * Extra Methods
     **********************************************/

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /*case R.id.donate:
                Intent goToDonateActivity = new Intent(MainScreenActivity.this, DonateActivity.class);
                startActivity(goToDonateActivity);
                break;
            case R.id.rate:
                Intent rateIntent = new Intent(Intent.ACTION_VIEW);
                rateIntent.setData(Uri.parse("market://details?id=com.satan.wtfismypostalcode"));
                startActivity(rateIntent);
                break;
            case R.id.contact:
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:sonu4414@gmail.com"));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "What's My Postal Code App");
                startActivity(Intent.createChooser(emailIntent, "Email with..."));
                break;
            case R.id.help:
                showHelpDialog();
                break;*/
        }
        return super.onOptionsItemSelected(item);
    }
}
