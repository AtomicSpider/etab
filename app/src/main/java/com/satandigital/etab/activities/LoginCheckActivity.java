package com.satandigital.etab.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.satandigital.etab.ETabApp;
import com.satandigital.etab.R;

import java.util.Date;

/**
 * Project : eTab
 * Created by Sanat Dutta on 4/28/2016.
 */
public class LoginCheckActivity extends Activity {

    private String TAG = "LoginCheckActivity";

    //Data
    Firebase myFirebaseRef;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myFirebaseRef = new Firebase(getResources().getString(R.string.firebase_url));
        goToAppropriateActivity();

    }

    private void goToAppropriateActivity() {
        AuthData mCurrentAuth = myFirebaseRef.getAuth();
        if (mCurrentAuth != null) {
            Date currDate = new Date();
            Date expDate = new Date(mCurrentAuth.getExpires() * 1000);
            Log.i(TAG, "Session expires on: " + expDate);
            long sessionPeriod = expDate.getTime() - currDate.getTime();

            if (sessionPeriod < 0) {
                Log.i(TAG, "Session expired, logging out.");
                myFirebaseRef.unauth();
                Intent mIntent = new Intent(this, LoginActivity.class);
                startActivity(mIntent);
            } else {
                if (mCurrentAuth.getProvider().equals("password")) {
                    ETabApp.userID = mCurrentAuth.getUid();
                    Log.i(TAG, "User Exists, userID:" + ETabApp.userID + " -> Main Activity");
                    Intent mIntent = new Intent(this, MainActivity.class);
                    startActivity(mIntent);
                } else {
                    Log.i(TAG, "User logged in without password. Provider: " + mCurrentAuth.getProvider() + " -> LogIn Activity");
                    myFirebaseRef.unauth();
                    Intent mIntent = new Intent(this, LoginActivity.class);
                    startActivity(mIntent);
                }
            }
        } else {
            Log.i(TAG, "User Does Not Exists, -> LogIn Activity");
            Intent mIntent = new Intent(this, LoginActivity.class);
            startActivity(mIntent);
        }
    }
}
